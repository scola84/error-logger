module.exports = (manager) => {
  return function log(error, request, response, next) {
    if (error.category) {
      const logger = manager.getLogger(error.category);

      if (logger) {
        logger.error(
          error.code +
          ' ' + error.message +
          (error.origin ? ' - ' + error.origin.message : '') +
          (error.detail ? ' - ' + JSON.stringify(error.detail) : '')
        );
      }
    }

    next(error);
  };
};
